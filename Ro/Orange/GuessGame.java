package Ro.Orange;

import java.util.Random;

public class GuessGame {

    public GuessGame() {
        startGame();
                }

    public String startGame() {
        int numberToGuess;
        Random number = new Random();
        numberToGuess = number.nextInt(10);
        System.out.println("I'm thinking about number: " + numberToGuess + ".");

        //i'm thinking about the number
        int G1;
        int G2;
        int G3;



        //i invite 3 players

        Player Alex = new Player();
        Player Bus = new Player();
        Player Albus = new Player();

        G1=Alex.guess();
        G2=Bus.guess();
        G3=Albus.guess();

            System.out.println("Alex: i'm guessing " + G1);
            System.out.println("Bus: i'm guessing " + G2);
            System.out.println("Albus: i'm guessing " + G3);
            System.out.println("Alex guessed: "+G1);
            System.out.println("Bus guessed: "+G2);
            System.out.println("Albus guessed: "+G3);
            if (numberToGuess == G1) {
                System.out.println("We have a winner!");
                System.out.println("Alex got it right? Yes!");
                System.out.println("Bus got it right? No!");
                System.out.println("Albus got it right? No!");
                System.out.println("The game is over!");
                return "stop";
            } else if (numberToGuess == G2) {
                System.out.println("We have a winner!");
                System.out.println("Alex got it right? No!");
                System.out.println("Bus got it right? Yes!");
                System.out.println("Albus got it right? No!");
                System.out.println("The game is over!");
                return "stop";
            } else if (numberToGuess == G3) {
                System.out.println("We have a winner!");
                System.out.println("Alex got it right? No!");
                System.out.println("Bus got it right? No!");
                System.out.println("Albus got it right? Yes!");
                System.out.println("The game is over!");
                return "stop";
            } else {
                System.out.println("Players will have to try again!");
                return "play";
            }
        }


    }



